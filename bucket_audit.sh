#!/bin/sh
#
show_usage()
{
  echo -----------------------------------------------------------------------
  echo To run the script:
  echo "  $0 <bucket_name>"
  echo
  echo Please set the two environment variables used in the script before running it:
  echo " 1.  <your_account_id> IBMCLOUD_ACCOUNT_GUID"
  echo " 2.  <your_apikey>     IBMCLOUD_API_KEY"
  echo -----------------------------------------------------------------------
}

bucket_name=$1

if [ $# -ne 1 -o -z "$IBMCLOUD_API_KEY" -o -z "$IBMCLOUD_ACCOUNT_GUID" ]
then
   show_usage
   exit 1
fi

accountID="${IBMCLOUD_ACCOUNT_GUID}"

################################################################################################
# Get the IAM token
################################################################################################
echo Fetching new cloud access token
ACCESS_TOKEN=$(curl --location --request POST 'https://iam.cloud.ibm.com/identity/token' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=urn:ibm:params:oauth:grant-type:apikey' \
  --data-urlencode "apikey=${IBMCLOUD_API_KEY}" | jq -r '.access_token')

if [ "$ACCESS_TOKEN" = "null" -o -z "$ACCESS_TOKEN" ]
then
  echo Failed to retrieve IAM token
  exit 1
fi

################################################################################################
# Get the COS instance service id of the bucket
################################################################################################
bucket_json=$(curl https://config.cloud-object-storage.cloud.ibm.com/v1/b/${bucket_name} -H "authorization: bearer $ACCESS_TOKEN")

if [ "$bucket_json" = "" ] ; then
  echo "Could not find COS bucket metadata for: $bucket_name"
  exit 1
fi

instanceID=$(echo $bucket_json | jq -r ".service_instance_id")

if [ "$instanceID" = "null" ]
then
  echo Failed to get COS instance ID
  exit 1
fi

################################################################################################
# Get authz report
################################################################################################
bucket_authz=$(curl --location --request PUT 'https://iam.cloud.ibm.com/v2/authz/resources' \
  --header 'Accept: application/vnd.authz.resources.v2+json' \
  --header "Authorization: Bearer $ACCESS_TOKEN" \
  --header 'Content-Type: application/json' \
  --header 'Accept-Language: en' \
  --header 'X-Request-Count: 1' \
  --data-raw '[
    {
      "attributes":    {
        "accountId": "'${IBMCLOUD_ACCOUNT_GUID}'",
        "serviceName": "cloud-object-storage",
        "serviceInstance": "'${instanceID}'",
        "resource": "'${bucket_name}'",
        "resourceType": "bucket"
      }
    }
]')

index=0
found_bucket_policies=0
subjectNo=`echo $bucket_authz | jq -r '.responses[].access' | jq 'length'`

echo ======================================================================
echo "Number of subjects found for COS bucket $bucket_name (COS instance $instanceID): $subjectNo"

if [ $subjectNo -lt 1 ]
then
  echo No subject found for the COS bucket
  exit 0
fi

echo List subjects for bucket: $bucket_name
while [ true ]
do
   if [ $index -eq $subjectNo ]
   then
      break
   fi

   # loop through subjects
   subjects=`echo $bucket_authz | jq -r --arg index "$index" '.responses[].access[$index | tonumber].subject[]'`
   accessid=`echo $subjects | jq -r '.id'`
   serviceName=`echo $subjects | jq -r '.serviceName'`
   resourceType=`echo $subjects | jq -r '.resourceType'`
   accountId=`echo $subjects | jq -r '.accountId'`
   serviceInstance=`echo $subjects | jq -r '.serviceInstance'`
   roles=`echo $bucket_authz | jq -r --arg index "$index" '.responses[].access[$index | tonumber].roleActions[].role.crn' | cut -d':' -f10` # must use crn, displayName may change in future

   if [ "$roles" = "" ]
   then
      roles=`echo $bucket_authz | jq -r --arg index "$index" '.responses[].access[$index | tonumber].platformExtensions.roleActions[].role.crn' | cut -d':' -f10`
   fi

   accessType="Unknown"
   index=`expr $index + 1`
   
   found_bucket_policies=`expr $found_bucket_policies + 1`

   echo ----------------------- Bucket access $found_bucket_policies ------------------------
   if [ "`echo $accessid | cut -d'-' -f1`" = "AccessGroupId" ]
   then
      accessType="Access Group"
      access_name=`ibmcloud iam access-groups --output json | jq -r ".[] | select(.id == \"$accessid\") | .name"`
      members=`ibmcloud iam access-group-users $access_name --output json | jq -r '.[].email'`
      serviceids=`ibmcloud iam access-group-service-ids $access_name --output json | jq -r .[].id`

   elif [ "`echo $accessid | cut -d'-' -f1`" = "iam" ] && [ "`echo $accessid | cut -d'-' -f2`" = "ServiceId" ]
   then
      accessType="Service ID"
      accessid=`echo $accessid | awk -F'iam' '{print substr($2,2)}'`
      access_name=`ibmcloud iam service-ids --output json | jq -r ".[] | select(.id == \"$accessid\") | .name"`

   elif [ "`echo $accessid | cut -d'-' -f1`" = "IBMid" ]
   then
      accessType="IBM User"
      [ "$account_users" = "" ] && account_users="$(ibmcloud account users --output json)"
      access_name=`echo $account_users | jq -r ".[] | select( .ibmUniqueId == \"$accessid\") | .firstname, .lastname, .userId")`

   elif [ "`echo $accessid | cut -d'-' -f1`" = "BSS" ]
   then
      accessType="Pending IBM User"
      [ "$account_users" = "" ] && account_users="$(ibmcloud account users --output json)"
      access_name=`echo $account_users | jq -r ".[] | select( .ibmUniqueId == \"$accessid\") | .firstname, .lastname, .userId")`

   elif [ "$accessid" = "null" ] && [ ! "$serviceName" = "null" ]
   then
      accessType="Resource"
      access_name=""   # Resource Instance name (to be improved)
      accessid=$serviceInstance

   fi

   echo "Access Type: " $accessType
   echo "Name: " $access_name;access_name=
   echo "Id: " $accessid;accessid=
   if [ "$accessType" = "Access Group" ]
   then
      echo Members:
      for member in $members; do echo "   " $member; done
      echo Service IDs:
      for iam_id in $serviceids
      do 
         serviceid=`echo $iam_id | awk -F'iam' '{print substr($2,2)}'`
         serviceid_name=`ibmcloud iam service-ids --output json | jq -r ".[] | select(.id == \"$serviceid\") | .name"`
         echo "   $serviceid: $serviceid_name"
      done
      members=

   elif [ "$accessType" = "Resource" ]
   then
      echo "Service Name: " $serviceName;serviceName=
      echo "Resource Type: " $resourceType;resourceType=
      echo "Account ID: " $accountId;accountId=

   fi

   echo Roles: $roles
   echo

done


echo Total number of subjects found for bucket $bucket_name: $found_bucket_policies
echo ----------------------------------------------------------------------

################################################################################################

