

# COS Bucket Audit

The script was written to answer the question: Who has access to a specific COS Bucket?

It reports all access types, both explicitly assigned and implicitly derived, including the following:

1. Service IDs
2. Access Groups
3. IBM Users
4. Pending IBM Users
5. Resources (serviceName and instance) applicable to service to service polices


## Tools used

1. IBM Cloud IAM Policy Decision API (no public documentation available as of now)
2. IBM Cloud CLI
3. jq - Command-line JSON processor


## Usage

- Log into the IBM Cloud with an 'ibmcloud login' command

- The command to run the script:  
     bucket_audit <bucket_name>  

- Two environment variables need to be set before running the script:  
  1.  <your_account_id> IBMCLOUD_ACCOUNT_GUID  
  2.  <your_apikey>     IBMCLOUD_API_KEY  


## Example run

./bucket_audit.sh test-123-abc  

Number of subjects found for COS bucket test-123-abc (COS instance cfc44885-4f98-4a20-8b08-ea903dac0985): 7  
List subjects for bucket: test-123-abc  
Bucket access 1  
Access Type:  IBM User  
Name:  qing ye qing.ye@uk.ibm.com  
Id:  IBMid-110000SNEK  
Roles: ContentReader Manager  
  
Bucket access 2  
Access Type:  Resource  
Name:  
Id:  null  
Service Name:  codeengine  
Resource Type:  null  
Account ID:  160423a63be746c18d6d061029699462  
Roles: NotificationsManager  
  
Bucket access 3  
Access Type:  Resource  
Name:  
Id:  null  
Service Name:  functions  
Resource Type:  null  
Account ID:  160423a63be746c18d6d061029699462  
Roles: NotificationsManager  
  
Bucket access 4  
Access Type:  Access Group  
Name:  test_group1  
Id:  AccessGroupId-9697f85f-7a5f-4e31-9931-2554067cc7e8  
Members:  
    qing.ye@uk.ibm.com  
Service IDs:  
   ServiceId-c8da9837-5589-486b-8b23-dc1142543ca1: Service-ID-Test1  
Roles: ObjectReader Writer  

Bucket access 5  
Access Type:  Service ID  
Name:  Service credentials-1  
Id:  ServiceId-27ac9379-ef1c-4a63-8ee6-75411d1b3427  
Roles: Manager  
  
Bucket access 6  
Access Type:  Service ID  
Name:  cos-standard-wh2  
Id:  ServiceId-722d8a14-d8a2-42a8-8b9f-a60dcbbebac0  
Roles: Manager  
  
Bucket access 7  
Access Type:  Pending IBM User  
Name:  abc1234@ibm.com  
Id:  BSS-d321f87c5cc1fb189637af89916a634b  
Roles: Reader  
  
Total number of subjects found for bucket test-123-abc: 7  
